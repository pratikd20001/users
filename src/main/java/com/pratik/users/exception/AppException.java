/**
 * 
 */
package com.pratik.users.exception;

import lombok.Builder;
import lombok.Data;

/**
 * @author Pratik Das
 *
 */
@Data
@Builder
public class AppException extends RuntimeException{
	
	private static final long serialVersionUID = 1L;
	private String errorCode;
	private String description;
	private Throwable ex;

}

/**
 * 
 */
package com.pratik.users.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import com.pratik.users.dtos.ApiErrorResponse;

/**
 * @author Pratik Das
 *
 */
@RestControllerAdvice
public class ApiExceptionHandler {
	
	@ExceptionHandler(AppException.class)
    public ResponseEntity<ApiErrorResponse> handleApiException(
        AppException ex) {
        ApiErrorResponse response = 
            ApiErrorResponse.builder().build();
        return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
    }

}

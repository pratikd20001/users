/**
 * 
 */
package com.pratik.users.controllers;

import javax.websocket.server.PathParam;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.pratik.users.dtos.DeviceDto;
import com.pratik.users.services.DeviceManagementService;

import lombok.extern.slf4j.Slf4j;

/**
 * @author Pratik Das
 *
 */
@RestController
@RequestMapping("/devices")
@Slf4j
public class DevicesController {
	
	private DeviceManagementService deviceManagementService;
	
	
	@Autowired
	public DevicesController(final DeviceManagementService deviceManagementService) {
		super();
		this.deviceManagementService = deviceManagementService;
	}

	
	@PutMapping("/")
	public DeviceDto updateDevice(final DeviceDto request) {
		return deviceManagementService.updateDevice(request);
		
	}
	
	@PostMapping("/")
	public DeviceDto createDevice(final DeviceDto request) {
		return deviceManagementService.createDevice(request);
		
	}
	
	@GetMapping("/{deviceID}")
	public DeviceDto getDevice(@PathParam("deviceID") final String deviceID) {
		final DeviceDto request = DeviceDto.builder().deviceID(deviceID).build();
		return deviceManagementService.getDevice(request);
	}
	
	
	@DeleteMapping("/")
	public DeviceDto deleteDevice(final DeviceDto request) {
		return deviceManagementService.deleteDevice(request);
		
	}
	
	
	
	
	
	

}

/**
 * 
 */
package com.pratik.users.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.pratik.users.dtos.SendOtpRequest;
import com.pratik.users.dtos.SendOtpResponse;
import com.pratik.users.dtos.UserCreateRequest;
import com.pratik.users.dtos.UserCreateResponse;
import com.pratik.users.dtos.VerifyCustomerWithOtpRequest;
import com.pratik.users.dtos.VerifyCustomerWithOtpResponse;
import com.pratik.users.services.RegisterCustomerService;
import com.pratik.users.services.UsersCreationService;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.extern.slf4j.Slf4j;

/**
 * @author Pratik Das
 *
 */
@RestController
@RequestMapping("/users")
@Slf4j
public class UsersController {
	
	private RegisterCustomerService registerCustomerService;
	private UsersCreationService usersCreationService;
	
	
	@Autowired
	public UsersController(RegisterCustomerService registerCustomerService, UsersCreationService usersCreationService) {
		super();
		this.registerCustomerService = registerCustomerService;
		this.usersCreationService = usersCreationService;
	}




	
	@PutMapping("/otp/verify")
	public VerifyCustomerWithOtpResponse verifyCustomerWithOtp(final VerifyCustomerWithOtpRequest request) {
		return registerCustomerService.verifyCustomerWithOtp(request);
		
	}
	
	@PostMapping("/otp/send")
	public SendOtpResponse sendOTP(final SendOtpRequest request) {
		return registerCustomerService.sendOtp(request);
		
	}
	
	@ApiOperation(value="Used to create user. MPin field should be hashed")
	@ApiResponses(value = {
	        @ApiResponse(code = 201, message = "Successfully created user"),
	        @ApiResponse(code = 401, message = "You are not authorized "),
	        @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
	        @ApiResponse(code = 404, message = "The resource you were trying to reach is not found")
	}
	)
	@PostMapping("/")
	public UserCreateResponse createUser(final UserCreateRequest request) {
		return usersCreationService.createUser(request);
		
	}
	
	
	

}

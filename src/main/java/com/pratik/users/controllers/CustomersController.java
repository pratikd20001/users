/**
 * 
 */
package com.pratik.users.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.pratik.users.dtos.VerifyCustomerRequest;
import com.pratik.users.dtos.VerifyCustomerResponse;
import com.pratik.users.services.RegisterCustomerService;

import lombok.extern.slf4j.Slf4j;

/**
 * @author Pratik Das
*
*/
@RestController
@RequestMapping("/customer")
@Slf4j
public class CustomersController {
	private RegisterCustomerService registerCustomerService;
	
	
	@Autowired
	public CustomersController(RegisterCustomerService registerCustomerService) {
		super();
		this.registerCustomerService = registerCustomerService;
	}



	@PutMapping("/verify")
	public VerifyCustomerResponse verifyCustomer(final VerifyCustomerRequest request) {
		
		return registerCustomerService.verifyCustomer(request);
		
		
		
	}
}

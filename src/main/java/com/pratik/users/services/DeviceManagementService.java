/**
 * 
 */
package com.pratik.users.services;

import org.springframework.stereotype.Service;

import com.pratik.users.dtos.DeviceDto;

import lombok.extern.slf4j.Slf4j;

/**
 * @author Pratik Das
 *
 */
@Service
@Slf4j
public class DeviceManagementService {



	public DeviceDto createDevice(final DeviceDto request) {
		// TODO Auto-generated method stub
		return null;
	}

	public DeviceDto updateDevice(DeviceDto request) {
		// TODO Auto-generated method stub
		return null;
	}

	public DeviceDto getDevice(final DeviceDto request) {
		// TODO Auto-generated method stub
		return DeviceDto.builder()
				.deviceID("D111")
				.model("Galaxy")
				.make("samsung")
				.build();
	}

	public DeviceDto deleteDevice(DeviceDto request) {
		// TODO Auto-generated method stub
		return null;
	}

}

/**
 * 
 */
package com.pratik.users.services;

import org.springframework.stereotype.Service;

import com.pratik.users.dtos.SendOtpRequest;
import com.pratik.users.dtos.SendOtpResponse;
import com.pratik.users.dtos.UserCreateRequest;
import com.pratik.users.dtos.UserCreateResponse;
import com.pratik.users.dtos.VerifyCustomerRequest;
import com.pratik.users.dtos.VerifyCustomerResponse;
import com.pratik.users.dtos.VerifyCustomerWithOtpRequest;
import com.pratik.users.dtos.VerifyCustomerWithOtpResponse;
import com.pratik.users.exception.AppException;

/**
 * @author Pratik Das
 *
 */
@Service
public class RegisterCustomerService {

	public VerifyCustomerResponse verifyCustomer(VerifyCustomerRequest request) {
		
		//throw AppException.builder().build();
		return null;
	}
	
	public VerifyCustomerWithOtpResponse verifyCustomerWithOtp(final VerifyCustomerWithOtpRequest request) {
		return null;
		
	}

	public SendOtpResponse sendOtp(SendOtpRequest request) {
		// TODO Auto-generated method stub
		return null;
	}

	public UserCreateResponse createUser(UserCreateRequest request) {
		// TODO Auto-generated method stub
		return null;
	}

}

/**
 * 
 */
package com.pratik.users.dtos;

import lombok.Builder;
import lombok.Data;

/**
 * @author Pratik Das
 *
 */
@Data
@Builder
public class UserDto {
	private String cif;
	private String corporateID;
	private String mpin;
	private String sessionID;
}

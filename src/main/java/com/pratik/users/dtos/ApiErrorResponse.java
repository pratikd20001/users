/**
 * 
 */
package com.pratik.users.dtos;

import lombok.Builder;
import lombok.Data;

/**
 * @author Pratik Das
 *
 */
@Data
@Builder
public class ApiErrorResponse {
	private String error;
    private String message;

}

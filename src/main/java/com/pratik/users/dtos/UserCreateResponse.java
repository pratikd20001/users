/**
 * 
 */
package com.pratik.users.dtos;

import lombok.Builder;
import lombok.Data;

/**
 * @author Pratik Das
 *
 */
@Data
@Builder
public class UserCreateResponse {
	
	private String userID;
	
	private UserDto customerDto;
}

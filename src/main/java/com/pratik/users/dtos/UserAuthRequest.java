/**
 * 
 */
package com.pratik.users.dtos;

import lombok.Builder;
import lombok.Data;

/**
 * @author Pratik Das
 *
 */
@Data
@Builder
public class UserAuthRequest {
	
	private String corporateID;
	
	private String userID;
	
	private String password;
	
}

/**
 * 
 */
package com.pratik.users.dtos;

import lombok.Builder;
import lombok.Data;

/**
 * @author Pratik Das
 *
 */
@Data
@Builder
public class UserAuthResponse {
	
	private String loginToken;
	
	private String sessionID;
	
	private UserDto customerDto;
}

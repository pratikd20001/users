/**
 * 
 */
package com.pratik.users.dtos;

import lombok.Data;

/**
 * @author Pratik Das
 *
 */
@Data
public final class SendOtpRequest {
	private String corporateID;
	private String cif;
	private String passwd;
	private String otp;
	private String sessionID;
}

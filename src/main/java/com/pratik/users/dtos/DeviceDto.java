/**
 * 
 */
package com.pratik.users.dtos;

import lombok.Builder;
import lombok.Data;

/**
 * @author Pratik Das
 *
 */
@Data
@Builder
public class DeviceDto {
	private String deviceID;
	private String make;
	private String model;
	private String sessionID;
}

/**
 * 
 */
package com.pratik.users.dtos;

import lombok.Builder;
import lombok.Data;

/**
 * @author Pratik Das
 *
 */
@Data
@Builder
public class UserCreateRequest {
	
	private String corporateID;
	
	private String userID;
	
	private String password;
	
}
